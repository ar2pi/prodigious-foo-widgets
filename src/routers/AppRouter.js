import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import App from '../containers/App';
import HomePage from '../containers/HomePage';
import Widget1 from '../containers/Widget1';
import Widget2 from '../containers/Widget2';
import Widget3 from '../containers/Widget3';
import Widget4 from '../containers/Widget4';
import NotFoundPage from '../containers/NotFoundPage';

const AppRouter = () => (
    <Router>
        <App>
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route path="/widget-1" component={Widget1} />
                <Route path="/widget-2" component={Widget2} />
                <Route path="/widget-3" component={Widget3} />
                <Route path="/widget-4" component={Widget4} />
                <Route component={NotFoundPage} />
            </Switch>
        </App>
    </Router>
);

export default AppRouter;
