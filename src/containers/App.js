import React from 'react';
import 'normalize.css';
import '../styles/main.scss';

const App = (props) => (
    <main id="app">
        {props.children}
    </main>
);

export default App;
