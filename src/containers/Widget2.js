import React, { Component } from 'react';
import ContactsInput from 'react-tagsinput';
import { Card, Button, Checkbox, Form, Input, TextArea, Message, Modal } from 'semantic-ui-react';
import validator from 'validator';
import withTranslation from '../wrappers/withTranslation';

class Widget2 extends Component {
    state = {
        contacts: ['one@mail.com', 'two@mail.com'],
        subject: '',
        message: '',
        errors: [],
        submit: false
    };

    handleContactInput = (contacts, curContact, index) => {
        if (validator.isEmail(curContact[0])) {
            this.setState({ contacts, errors: [] });
        } else {
            this.setState({ errors: [...this.state.errors, this.props.__('Invalid email')] });
        }
    };

    handleSubjectInput = (event, element) => {
        this.setState(() => ({ subject: element.value }));
    };

    handleMessageInput = (event, element) => {
        this.setState(() => ({ message: element.value }));
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let errors = [];
        if (validator.isEmpty(this.state.subject)) {
            errors.push(this.props.__('Please do provide a subject for you email'));
        }
        if (validator.isEmpty(this.state.message)) {
            errors.push(this.props.__('Forgot the body message mebbe ?'));
        }
        if (this.state.contacts.length < 1) {
            errors.push(this.props.__('And add some contacts...'));
        }
        if (errors.length > 0) {
            this.setState(() => ({ errors }));
        } else {
            this.setState(() => ({ submit: true, errors: [], subject: '', message: '' }));
        }
    };

    handleModalClose = () => {
        this.setState(() => ({ submit: false }));
    };

    render() {
        return (
            <div className="widget layout__center--full-height">
                <Card>
                    <Card.Content>
                        <Form error={this.state.errors.length > 0}>
                            <Message error>
                                <Message.Header>{this.props.__('Gotta fix those errors!')}</Message.Header>
                                <Message.Content>
                                    <ul>
                                        {this.state.errors.map((error, i) => (
                                            <li key={i}>{error}</li>
                                        ))}
                                    </ul>
                                </Message.Content>
                            </Message>
                            <Form.Field>
                                <label>{this.props.__('Contacts')}</label>
                                <ContactsInput
                                    value={this.state.contacts}
                                    inputProps={{ placeholder: this.props.__('Add an email') }}
                                    onChange={this.handleContactInput}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{this.props.__('Subject')}</label>
                                <Input
                                    type="text"
                                    placeholder={this.props.__('You can add a subject')}
                                    value={this.state.subject}
                                    onChange={this.handleSubjectInput}
                                    autoFocus
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>{this.props.__('Message')}</label>
                                <TextArea
                                    placeholder={this.props.__('Leave your message here')}
                                    value={this.state.message}
                                    onChange={this.handleMessageInput}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Checkbox label={this.props.__('Save a copy')} />
                                <Button
                                    type="submit"
                                    floated="right"
                                    color="red"
                                    onClick={this.handleSubmit}
                                >
                                    {this.props.__('Send Mail')}
                                </Button>
                            </Form.Field>
                        </Form>
                    </Card.Content>
                </Card>
                <Modal open={this.state.submit} onClose={this.handleModalClose}>
                    <Modal.Header>{this.props.__('Success!')}</Modal.Header>
                    <Modal.Content>{this.props.__('Nicely done, your message as been sent!')}</Modal.Content>
                    <Modal.Actions>
                        <Button primary onClick={this.handleModalClose}>
                            {this.props.__('Ok')}
                        </Button>
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }
}

export default withTranslation(Widget2);
