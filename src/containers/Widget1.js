import React, { Component } from 'react';
import axios from 'axios';
import { Card, Grid, Icon, Image } from 'semantic-ui-react';
import Skeleton from '../components/Loading/Skeleton';
import withLoadingState from '../wrappers/withLoadingState';
import withTranslation from '../wrappers/withTranslation';
import { xhrState } from '../actions/xhr';

class Widget1 extends Component {
    state = {
        liked: false,
        profile: {}
    };

    handleLike = () => {
        this.setState((prevState) => {
            if (!prevState.liked) {
                return {
                    likeCount: ++prevState.profile.likeCount,
                    liked: true
                };
            }
        });
    };

    componentDidMount() {
        this.props.dispatch(xhrState('loading'));
        // mocking some crappy response time here
        setTimeout(() => {
            axios.get('/data/profile-john.json').then((res) => {
                this.setState(() => ({ profile: res.data }));
                this.props.dispatch(xhrState('loaded'));
            });
        }, 3000);
    }

    render() {
        return (
            <div className="widget layout__center--full-height">
                <Card>
                    {this.props.loading ? (
                        <Skeleton height={8} />
                    ) : (
                        <Image src={this.state.profile.cover} />
                    )}
                    {this.props.loading ? (
                        <Card.Content>
                            <Skeleton height={3} />
                        </Card.Content>
                    ) : (
                        <Card.Content>
                            <Image
                                className="card__profile-image"
                                floated="left"
                                size="mini"
                                src={this.state.profile.img}
                            />
                            <Card.Header>{this.state.profile.name}</Card.Header>
                            <Card.Description className="card__description">
                                {this.props.__(this.state.profile.description)}
                            </Card.Description>
                        </Card.Content>
                    )}
                    <Card.Content extra className="card__bottom-actions">
                        <Grid columns={3}>
                            <Grid.Column>
                                {this.props.loading ? (
                                    <Skeleton height={1} />
                                ) : (
                                    <a className="card__action">
                                        <Icon name="eye" />
                                        {this.state.profile.viewCount}
                                    </a>
                                )}
                            </Grid.Column>
                            <Grid.Column>
                                {this.props.loading ? (
                                    <Skeleton height={1} />
                                ) : (
                                    <a className="card__action">
                                        <Icon name="comment" />
                                        {this.state.profile.commentCount}
                                    </a>
                                )}
                            </Grid.Column>
                            <Grid.Column>
                                {this.props.loading ? (
                                    <Skeleton height={1} />
                                ) : (
                                    <a
                                        className={`card__action--like ${this.state.liked &&
                                            'active'}`}
                                        onClick={this.handleLike}
                                    >
                                        <Icon name="like" />
                                        {this.state.profile.likeCount}
                                    </a>
                                )}
                            </Grid.Column>
                        </Grid>
                    </Card.Content>
                </Card>
            </div>
        );
    }
}

export default withTranslation(withLoadingState(Widget1));
