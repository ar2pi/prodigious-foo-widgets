import '../services/Modernizr';
import raf from 'raf';
import injectTapEventPlugin from 'react-tap-event-plugin';

// simple polyfill interface w/ code splitting and method piping ftw
// you can import { _raf } from Polyfills and _raf()
// or just import Polyfill and Polyfill.raf().localStorage()...

export const _raf = () => {
    if (!window.Modernizr.requestanimationframe) {
        raf.polyfill();
    }
};

export const _localStorage = () => {
    if (!window.Modernizr.localstorage) {
        window.alert('Pas de bras, pas de chocolat :3');
    }
};

export const _tapEvent = () => {
    injectTapEventPlugin();
};

class Polyfill {
    raf = () => _raf() || this
    localStorage = () => _localStorage() || this
    tapEvent = () => _tapEvent() || this
}

export default new Polyfill();