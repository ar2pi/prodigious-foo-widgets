# Prodigious widgets

See it in action -> https://prodigious-widgets.herokuapp.com

### Install / start / build

```yarn install```  
```yarn run start```  
```yarn run build```

### Uses

1. react 16
2. semantic-ui-react
3. redux
4. britecharts-react
5. axios
6. scss

Why ? 

1. Because, hype
2. Let components communicate between each other (loading state, translation, trigger animations, etc.)
3. Looks good and had in mind how to use to get desired output (cards, grids, form components, etc.)
4. Integrating D3 and React can be tricky, needed a library that allowed to bootstrap some graphs real fast while still corresponding to what i had in mind. This particular one proved to be interesting, although a more customized approach would be preferable
5. Because fetch is nice, but still feels hacky / experimental (having to reparse json and not getting expected error responses errr), axios still does the job with elegance
6. Some custom styling was needed... lots of ugly stuff down there in order to override semantic's css, could be fixed with proper theming and better encapsulation / specificity of custom components

### Features

- Loading state while fetching external (currently mocked) data **(widget 1, 3, 4)**
- Translation HOC with custom yaml files to map string patterns, enabling quick data retrieval by simple object indexation. Yaml file because... easier to handle for translation purposes imho. Also gets stored in localstorage, could have used cookie though **(widget 1, 2, 3 , 4)**
- Form validation **(widget 2)**
- Some custom animation effects triggered from state by redux **(widget 3)**
- The like button **(widget 1)**